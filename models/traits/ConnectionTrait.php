<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models\traits;

/**
 * трейт используеться для добавления в модели yii загрузки класса микротика
 * @author sinelnikof
 */
trait ConnectionTrait {

    /**
     * загрузка классов для создания динамического подключения
     * исползуя название создаем экземпляр класса
     * @param string $mikrotikClassName 
     * @return \app\models\traits\mikrotikClassName
     */
    public function loadNodeModel($mikrotikClassName) {
        $mikrotik = new $mikrotikClassName;
        $mikrotik->connection($this);
        return $mikrotik;
    }

}
