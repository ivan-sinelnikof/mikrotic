<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "node".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $password
 * @property string|null $host
 * @property string|null $login
 * @property string|null $port
 * @property string|null $description
 * @property string|null $ser_num
 */
class Node extends \yii\db\ActiveRecord {

    use traits\ConnectionTrait;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'node';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
                [['name', 'description'], 'string'],
                [['password', 'host', 'login', 'port', 'ser_num'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'password' => 'Password',
            'host' => 'Host',
            'login' => 'Login',
            'port' => 'Port',
            'description' => 'Description',
            'ser_num' => 'Ser Num',
        ];
    }

}
