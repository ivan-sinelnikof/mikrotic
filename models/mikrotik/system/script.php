<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models\mikrotik\system;

use app\models\mikrotik\BasicModel;

/**
 * Description of script
 *
 * @author sinelnikof
 */
class script extends BasicModel {

    public $id;
    public $name;
    public $owner;
    public $policy;
    public $dont_require_permissions;
    public $last_started;
    public $run_count;
    public $source;
    public $invalid;

    public function rules() {
        return [
                [['id',
            'name',
            'owner',
            'policy',
            'dont_require_permissions',
            'last_started',
            'run_count',
            'source',
            'invalid',
                ], 'safe'],
        ];
    }

    public function attributeLabels() {
        return [
            'address' => 'Адрес',
            'network' => 'Сеть',
            'interface' => 'Интерфейс',
        ];
    }

    public static function remoteNames() {
        return [
            '.id' => 'id',
            'name' => 'name',
            'owner' => 'owner',
            'policy' => 'policy',
            'dont-require-permissions' => 'dont_require_permissions',
            'last-started' => 'last_started',
            'run-count' => 'run_count',
            'source' => 'source',
            'invalid' => 'invalid',
        ];
    }

}
