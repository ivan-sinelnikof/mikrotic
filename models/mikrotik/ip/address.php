<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models\mikrotik\ip;

use Yii;
use app\models\mikrotik\BasicModel;

/**
 * Description of address
 *
 * @author sinelnikof
 */
class address extends BasicModel {

    public $id;
    public $address;
    public $network;
    public $interface;
    public $invalid;
    public $dynamic;
    public $disabled;
    public $actual_interface;

    public function rules() {
        return [
                [['id', 'address', 'network', 'interface', 'invalid'], 'safe'],
        ];
    }

    public function attributeLabels() {
        return [
            'address' => 'Адрес',
            'network' => 'Сеть',
            'interface' => 'Интерфейс',
        ];
    }

    public static function remoteNames() {
        return [
            '.id' => 'id',
            'address' => 'address',
            'network' => 'network',
            'interface' => 'interface',
            'invalid' => 'invalid',
            'dynamic' => 'dynamic',
            'disabled' => 'disabled',
            'actual-interface' => 'actual_interface',
        ];
    }

}
