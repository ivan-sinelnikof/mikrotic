<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models\mikrotik\ip\firewall;

/**
 * Description of connection
 *
 * @author sinelnikof
 */
class connection extends \app\models\mikrotik\BasicModel {

    public $id;
    public $protocol;
    public $src_address;
    public $dst_address;
    public $reply_src_address;
    public $reply_dst_address;
    public $tcp_state;
    public $timeout;
    public $orig_packets;
    public $orig_bytes;
    public $orig_fasttrack_packets;
    public $orig_fasttrack_bytes;
    public $repl_packets;
    public $repl_bytes;
    public $repl_fasttrack_packets;
    public $repl_fasttrack_bytes;
    public $orig_rate;
    public $repl_rate;
    public $expected;
    public $seen_reply;
    public $assured;
    public $confirmed;
    public $dying;
    public $fasttrack;
    public $srcnat;
    public $dstnat;
    public $icmp_type;
    public $icmp_code;

    public function rules() {
        return [
                [[
               'id'  ,   'protocol',
            'src_address',
            'dst_address',
            'reply_src_address',
            'reply_dst_address',
            'tcp_state',
            'timeout',
            'orig_packets',
            'orig_bytes',
            'orig_fasttrack_packets',
            'orig_fasttrack_bytes',
            'repl_packets',
            'repl_bytes',
            'repl_fasttrack_packets',
            'repl_fasttrack_bytes',
            'orig_rate',
            'repl_rate',
            'expected',
            'seen_reply',
            'assured',
            'confirmed',
            'dying',
            'fasttrack',
            'srcnat',
            'icmp-type',
            'icmp-code',
            'dstnat'], 'safe']
        ];
    }

    public static function remoteNames() {
        return [
            '.id' => 'id',
            'protocol' => 'protocol',
            'src-address' => 'src_address',
            'dst-address' => 'dst_address',
            'reply-src-address' => 'reply_src_address',
            'reply-dst-address' => 'reply_dst_address',
            'tcp-state' => 'tcp_state',
            'timeout' => 'timeout',
            'orig-packets' => 'orig_packets',
            'orig-bytes' => 'orig_bytes',
            'orig-fasttrack-packets' => 'orig_fasttrack_packets',
            'orig-fasttrack-bytes' => 'orig_fasttrack_bytes',
            'repl-packets' => 'repl_packets',
            'repl-bytes' => 'repl_bytes',
            'repl-fasttrack-packets' => 'repl_fasttrack_packets',
            'repl-fasttrack-bytes' => 'repl_fasttrack_bytes',
            'orig-rate' => 'orig_rate',
            'repl-rate' => 'repl_rate',
            'expected' => 'expected',
            'seen-reply' => 'seen_reply',
            'assured' => 'assured',
            'confirmed' => 'confirmed',
            'dying' => 'dying',
            'fasttrack' => 'fasttrack',
            'srcnat' => 'srcnat',
            'dstnat' => 'dstnat',
            'icmp-type' => 'icmp_type',
        ];
    }

}
