<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models\mikrotik;

use yii;

/**
 * @author sinelnikof
 * Базовая модель служит для расширения функционала моделей микротика
 *
 * концептуальная модель поведения 
 *       $model = new  \app\models\Node();
 * 
 *       $connectionInstance = $model->loadNodeModel(\app\models\mikrotik\ip\firewall\connection::class);
 *       $connections = $connectionInstance->where([
 *            'src_address' => '192.168.77.23',
 *            'tcp_state' => 'time-wait'
 *            ])->find()->all();
 * используя фильтры в where() сделали выборку и получили в 
 * $connections экземпляры классов \app\models\mikrotik\ip\firewall\connection 
 * расширенных для работы фреймворком те все методы + валидация геттеры сеттеры итд
 * 
 * 
 */
class BasicModel extends yii\base\Model {

    protected $searchParams = [];
    protected $client = [];
    protected $pathName = '';
    protected $query = '';
    protected $currentClassName = '';

    /**
     * создание соедененеия с оборудованием
     * @param \app\models\Node $node
     */
    public function connection(\app\models\Node $node) {

        $config = new \RouterOS\Config([
            'host' => $node->host,
            'user' => $node->login,
            'pass' => $node->password,
            'port' => $node->port,
        ]);
        $this->client = new \RouterOS\Client($config);
    }

    /**
     * в ините бкдем приобразовывать пути в название методов для апи микротика
     * приобразуем неймспейсы в пути для апи типа 
     *  /ip/firewall/connection/
     */
    public function init() {
        $this->currentClassName = get_called_class();
        $name = str_replace(__NAMESPACE__, '', $this->currentClassName);
        $this->pathName = str_replace('\\', '/', $name);
        return parent::init();
    }

    /**
     * Функция для создания параметров выборки по условию ключ=>значение
     * 
     * @param type $params параметры выборки ['address' => '192.168.77.1/24']
     * @return $this
     */
    public function where($params) {
        $model = new $this->currentClassName;
        $searchParams = [];
        foreach ($params as $key => $value) {
            $newKey = array_flip($model::remoteNames())[$key];
            $searchParams [$newKey] = $value;
        }
        $this->searchParams = $searchParams;
        return $this;
    }

    /**
     * создание маршрута и  параметров для обращения к оборудованию
     * @return $this
     */
    public function find() {
        $this->query = new \RouterOS\Query($this->pathName . '/print');
        foreach ($this->searchParams as $name => $value) {
            $this->query->where($name, $value);
        }
        return $this;
    }

    /**
     * тут отправляем сформированное обращение и выбираем все позиции
     * создаем экземпляр микротиковского класса  и добавляем в него параметры из ответа
     * используя массив внутри класса как мапер потому что в php нельзя называть поля типа 
     * adress-list нужно adress_list  
     * необходимо обращене обработать try .. так как оборудование может быть недоступно
     * @return \app\models\mikrotik\currentClassName
     */
    public function all() {
        $modelCollection = [];
        foreach ($this->client->query($this->query)->read() as $value) {
            $model = new $this->currentClassName;
            $attributes = [];
            foreach ($value as $itemKey => $itemValue) {
                $newKey = $model::remoteNames()[$itemKey];
                $attributes[$newKey] = $itemValue;
            }
            $model->attributes = ($attributes );
            $modelCollection [] = $model;
        };

        return $modelCollection;
    }

    /**
     * Возращает один экземпляр класса микротика
     * @return type
     */
    public function one() {
        return current($this->all());
    }

}
