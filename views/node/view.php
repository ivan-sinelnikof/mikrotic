<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Node */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Nodes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="node-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name:ntext',
                ['label' => 'password', 'value' => function ($model) {
                    return ('.....');
                }],
            'host',
            'login',
            'port',
            'description:ntext',
            'ser_num',
        ],
    ])
    ?>

    <?=
    yii\grid\GridView::widget([
        'dataProvider' => new yii\data\ArrayDataProvider([
            'allModels' => $script
                ]),
        'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
            'id',
            'name',
            'owner',
            'policy',
            'dont_require_permissions',
            'last_started',
            'run_count',
            'source',
            'invalid',
            //'description:ntext',
            //'ser_num',
         ],
    ]);
    ?>

  
    <?=
    yii\grid\GridView::widget([
        'dataProvider' => new yii\data\ArrayDataProvider([
            'allModels' => $address
                ]),
        'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
            'address',
            'network:ntext',
            'interface',
            'invalid',
            'dynamic',
            'disabled',
            //'description:ntext',
            //'ser_num',
         ],
    ]);
    ?>
     <?php \yii\widgets\Pjax::begin(['id' => 'countries']) ?>
   <?=
    yii\grid\GridView::widget([
        'dataProvider' => new yii\data\ArrayDataProvider([
            'allModels' => $connection
                ]),
        'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
         'id',    'protocol',
            'src_address',
            'dst_address',
            'reply_src_address',
            'reply_dst_address',
            'tcp_state',
            'timeout',
            'orig_packets',
            'orig_bytes',
            //'description:ntext',
            //'ser_num',
         ],
    ]);
    ?>

    <?php \yii\widgets\Pjax::end() ?>

</div>
  <?php
    $js = <<< JS
      $.pjax.defaults.timeout = false;       // For JS use case yor should manual override default timeout.
        setInterval(function () {
            $.pjax.reload({container: '#countries'});
        }, 1000)
JS;
    $this->registerJs($js);
    ?>